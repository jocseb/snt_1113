# Activité 1 : Coordonnées géographiques

## 1. Se repérer à la surface de la Terre

!!! example "Exercice 1 : coordonnées de villes du monde"
    === "Énoncé"
        Télécharger [ce document GeoGebra](data/geogebra_coord_calculatrice_3D_22022023.ggb){:target="_blank"} puis
        se rendre sur [le site GeoGebra](https://www.geogebra.org/calculator){:target="_blank"} et aller sur ![ouvrir](data/geogebra_ouvrir.png){width=10%} pour ouvrir le fichier téléchargé puis cliquer sur FICHIER LOCAL ![fichier local](data/geogebra_fichier_local.png){width=10%} et aller le chercher dans le dossier *Téléchargements*) pour obtenir ceci :  

        <figure markdown>
        ![](data/geogebra-export.png){:target="_blank"}
        <figcaption></figcaption>
        </figure>
        
        Sur l'animation GeoGebra apparaissent des villes (en noir) et un point bleu que l'on peut déplacer en maintenant appuyé le clic gauche de la souris.  
        Dans le tableau ci-dessous sont données des coordonnées de villes.  
        Associer à chaque ville le numéro qui lui correspond. 
     

        | numéro de la ville | Latitude | Longitude |
        |:---:|---|---|
        | 1 | 51° N | 0° O|
        | 2 | 49° N | 2° E|
        | 3 | 41° N | 116° O|
        | 4 | 0° N | 79° O|
        | 5 | 34° S | 18° E|
        | 6 | 34° S | 71° O|
        | 7 | 40° N | 74° E |

    === "Correction"
        Faire l'exercice avant !

        | numéro de la ville | Latitude | Longitude | Ville |
        |:---:|---|---|---|
        | 1 | 51° N | 0° O| Londres|
        | 2 | 49° N | 2° E| Paris|
        | 3 | 41° N | 116° O| New-York|
        | 4 | 0° N | 79° O| Quito|
        | 5 | 34° S | 18° E| Le Cap|
        | 6 | 34° S | 71° O| Santiago|
        | 7 | 40° N | 74° E | Pékin |

<!-- Cliquer [ici](../Theme2_Localisation_Cartographie/cours.md/#11-convention-décriture-dangle){. target="_blank"} -->
Revenir au cours et aller à la partie ``1.1. Les différentes conventions d'écriture d'angles``.

## 2. Les différentes conventions d'écriture d'angles

!!! example "Exercice 2"
	=== "Conversion"
	![](data/bordeaux.png){align=right}

	Numériquement parlant, le format décimal (DD) des coordonnées géographiques est le plus pratique.

	Sur le web (ou ailleurs), il arrive fréquemment que ces coordonnées ne soient pas données au format décimal, mais plutôt au format DMS (degrés, minutes, secondes).

	Par exemple, les coordonnées des villes sur Wikipedia sont données au format DMS (ci-contre celles de Bordeaux).

	Convertir les coordonnées de Bordeaux au format DD.
    === "Correction"
        Faire l'exercice avant !

	Coordonnées de Bordeaux :
	en DMS :  
	Latitude : 44°50’16” N  
	Longitude : 0°34’46” O  

	en DD :  
	Latitude : 44 + 50/60 + 16/3600
	= + 44.837778 °  
	Le signe + marque le fait que cette latitude est dans l’hémisphère Nord.

	Longitude : 0 + 34/60 + 46/3600
	= - 0.579444 °  
	Le signe − marque le fait que cette longitude est à l'ouest du méridien de Greenwich.

## 3. Utilisation du site ```coordonnees-gps```

Il existe des sites en ligne qui proposent de manipuler très facilement des coordonnées GPS et de trouver les lieux correspondants, comme [https://www.coordonnees-gps.fr/](https://www.coordonnees-gps.fr/){:target="_blank"} par exemple.

!!! example "Exercice 3"
    === "Énoncé"
        En utilisant le site ```coordonnees-gps.fr```, répondre aux 2 questions suivantes :  
        1. Que trouve-t-on aux coordonnées : 45.193867 , -0.747963 ?  (choisir la vue satellite et zoomer)  
        2. Que peut-on observer aux coordonnées  : 40° 41' 21.296'' N / 74° 2' 40.199'' O ?  

    === "Correction"
        Faire l'exercice avant !
        1. La salle C3 C4 du Lycée Odilon Redon de Pauillac.
        2. La statue de la Liberté à New-York.

Revenir au cours à la partie ``2. Comprendre la géolocalisation``.
